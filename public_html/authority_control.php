<?PHP

header('HTTP/1.1 200 OK');
header('Content-type: application/json; charset=utf-8');
//header('Content-type: text/plain; charset=utf-8');
header('Connection: close');

$opts = array(
 'http'=>array(
  'header' => 'Connection: close'
 )
);
$context = stream_context_create($opts);

$o['status'] = 'OK' ;
if ( $_REQUEST['query'] == 'viaf' ) {
	$key = $_REQUEST['key'] ;
	$id = $_REQUEST['id'] ;
	// $url = "https://viaf.org/en/viaf/search?query=local.names+all+%22" . urlencode($key) . "%22&maximumRecords=10&sortKeys=holdingscount&httpAccept=text/xml" ;
	// $o['result'] = file_get_contents ( $url , false , $context ) ;
	
	if ( isset($key) and trim($key)!='' ) { # Search query
		$url = 'https://viaf.org/api/search';
		$payload = [
			"reqValues" => ["field"=>"local.names","index"=>"viaf","searchTerms"=>$key],
			"meta" => ["env"=>"prod","pageIndex"=>0,"pageSize"=>10]
		];
	} else if ( isset($id) and trim($id)!='' ) { # Specific ID
		$url = 'https://viaf.org/api/cluster-record';
		$payload = [
			"reqValues" => ["recordId"=>$id,"isSourceId"=>false],
			"meta" => ["env"=>"prod","pageIndex"=>0,"pageSize"=>1]
		];
	} else {
		$o['status'] = "key or id required";
		if ( isset ( $_REQUEST['callback'] ) ) print $_REQUEST['callback']."(" ;
		print json_encode ( $o ) ;
		if ( isset ( $_REQUEST['callback'] ) ) print ");" ;
		print "\n" ;
		exit(0);
	}
	$jsonData = json_encode($payload);


	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
	    'Content-Type: application/json',
	    'Content-Length: ' . strlen($jsonData)
	));

	$response = curl_exec($ch);
	curl_close($ch);

	$o['result'] = json_decode($response);


} elseif ( $_REQUEST['query'] == 'gnd' ) {
	$key = $_REQUEST['key'] ;
	for ( $i = 0 ; $i < 1 ; $i++ ) {
		$t = file_get_contents ( "https://portal.dnb.de/opac.htm?method=showFullRecord&currentResultId=" . urlencode ( $key ) . "%26any%26persons&currentPosition=$i" ) ;
	}
}

if ( isset ( $_REQUEST['callback'] ) ) print $_REQUEST['callback']."(" ;
print json_encode ( $o ) ;
if ( isset ( $_REQUEST['callback'] ) ) print ");" ;
print "\n" ;
